---
layout: post
title: Docker学习笔记
date: 2017-6-16 19:24:41
tags: Docker,Dockerfile
categories: Docker
toc: true
description:
---

[TOC]

# Docker基本操作

## Docker简介

Docker 是一个开源的应用容器引擎，基于 [Go 语言](http://www.runoob.com/go/go-tutorial.html) 并遵从Apache2.0协议开源。

Docker 可以让开发者打包他们的应用以及依赖包到一个轻量级、可移植的容器中，然后发布到任何流行的 Linux 机器上，也可以实现虚拟化。

容器是完全使用沙箱机制，相互之间不会有任何接口（类似 iPhone 的 app）,更重要的是容器性能开销极低。

Docker的主要目标是“Build,Ship and Run Any App, Anywhere”，即通过对应用组件的封装（Packaging）、分发（Distribution）、部署（Deployment）、运行（Runtime）等生命周期的管理，达到应用组件级别的“一次封装，到处运行”（有点类似于java）。这里的应用组件，即可以是一个Web应用，也可以是一套数据库服务，甚至是一个操作系统或者编译器。
<!--more-->
## Docker的应用场景

- Web 应用的自动化打包和发布。
- 自动化测试和持续集成、发布
- 在服务型环境中部署和调整数据库或其他的后台应用
- 从头编译或者扩展现有的OpenShift或Cloud Foundry平台来搭建自己的PaaS环境

## Docker的优点

- 简化程序

Docker让开发者可以打包他们的应用以及依赖包到一个可移植的容器中，然后发布到任何流行的Linux机器上，便可以实现虚拟化。Docker改变了虚拟化的方式，使开发者可以直接将自己的成果放入Docker中进行管理。过去需要数天乃至数周的任务，在Docker容器的处理下，只需要数秒就能完成。

- 避免选择恐惧症

Docker镜像中包含了运行环境和配置，所以Docker可以简化部署多种应用实例工作。比如Web应用、后台应用、数据库应用、大数据应用比如Hadoop集群、消息队列等等都可以打包成一个镜像部署。

- 节省开支

云计算时代的到来，使开发者不必为了追求效果而配置高额的硬件，Docker改变了高性能必然高价格的思维定势。Docker与云的结合，让云空间得到更充分的利用，不仅解决了硬件管理的问题，也改变了虚拟化的方式。

## Centos的Docker安装与启动

- 检查Linux版本

```
[docker@localhost ~]$ uname -r
```

Docker要求Centos系统的内核版本高于3.10

- 安装Docker

切换到root用户，更新系统

```
[root@localhost ~]# yum update
```

安装Docker

```
[root@localhost ~]# yum -y install docker
```

Docker软件包和依赖包已经包含在默认的Centos-Extras软件源里了。

如果这种方式不能安装，也可使用下面的命令进行安装

```
[root@localhost ~]# curl -fsSL https://get.docker.com/ | sh
```

执行这个脚本后会添加docker.repo源并安装Docker

注：若安装失败，重新使用上面命令安装时有时会报错，只需要去家目录下的.docker目录中将docker的相关文件删除，然后重新执行命令下载即可。

- 启动Docker服务

```
[root@localhost ~]# service docker start
```

- 测试

```
[docker@localhost ~]$ docker run hello-world
```

由于本地没有hello-world这个镜像，所以会下载一个hello-world的镜像，并在容器中运行

> Docker的基本使用

- 查看Docker常用命令

```
[docker@localhost ~]$ docker
```

或者

```
[docker@localhost ~]$ docker --help
```

如我们需要查看其中某个命令的使用方法，可使用以下命令

```
[docker@localhost ~]$ docker run --help
```

- 运行一个web应用

我们在Docker容器中运行一个Python Flask应用来运行一个web应用

```
[docker@localhost ~]$ docker run -d -P training/webapp python app.py
```

我们先来看看之前执行docker run --help命令后的结果吧

```
docker run [OPTIONS] IMAGE [COMMAND] [ARG...]
```

OPTIONS: 代表run命令的一些参数

IMAGE: 镜像名

COMMAND: 运行镜像之后要执行的命令

ARG...: 命令需要的一些参数

好了，我们现在来看看刚刚我们运行一个web应用的命令

-d, --detach=false          Run container in background and print container ID

让容器在后台运行，默认是关闭的

-P, --publish-all=false     Publish all exposed ports to random ports

让容器内部使用的网络端口映射到我们使用的主机上，默认是关闭的

注意: 我们这里用的是大写的-P

小写的-p手动将容器端口映射到宿主机上的端口，如

```
[docker@localhost ~]$ docker run -d -p 5000:5000 training/webapp python app.py
```

### 查看WEB应用容器

#### 查看正在运行的容器

```
[docker@localhost ~]$ docker ps
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS                     NAMES
f5d5071807a8        training/webapp     "python app.py"     13 seconds ago      Up 12 seconds       0.0.0.0:32768->5000/tcp   prickly_davinci
```

#### 查看所有已构建的容器

~~~shell
docker ps -a
~~~

包括正在运行，已停止的等多个容器。

结果中有容器ID，镜像名，端口，容器名等信息，其中端口显示了prickly_davinci容器端口的映射情况，此时映射的端口是容器自动做的映射，如果我们运行时没使用-P，而是使用-p手动映射，此处则显示手动指定的端口。其次容器名称此处为容器自动指定的，我们可以通过--name来手动指定，如

```
[docker@localhost ~]$ docker run -d -p 5000:5000 --name webapp training/webapp python app.py
```

上面默认都是绑定tcp端口，如果要绑定UDP端口，可以在端口后面加上/udp

```
[docker@localhost ~]$ docker run -d -p 5000:5000/udp --name webapp training/webapp python app.py
```

#### 查看容器端口映射

- 使用容器ID查看容器端口映射情况

```
[docker@localhost ~]$ docker port f5d5071807a8
5000/tcp -> 0.0.0.0:32768
```

- 使用容器名称查看端口映射情况

```
[docker@localhost ~]$ docker port prickly_davinci
5000/tcp -> 0.0.0.0:32768
```

- 查看具体某个端口的映射情况

```
[docker@localhost docker]$ docker port tomcat 8080
0.0.0.0:8080
```

**接下来凡是使用容器标识操作的都使用容器名称，并且容器ID也支持相同的命令操**

#### 查看WEB应用程序日志

```
[docker@localhost ~]$ docker logs -f modest_banach
```

-f: 让docker logs像使用tail -f一样来输出容器内部的标准输出

#### 查看WEB应用程序容器的进程

```
[docker@localhost ~]$ docker top modest_banach
```
#### 检查WEB应用程序

```
[docker@localhost ~]$ docker inspect modest_banach
```
#### 停止WEB应用程序

```
[docker@localhost ~]$ docker stop modest_banach
```
#### 启动WEB应用容器

```
[docker@localhost ~]$ docker start modest_banach
```
#### 重启WEB应用容器

```
[docker@localhost ~]$ docker restart modest_banach
```

**注**：正在运行的容器我们可以使用restart来重启

#### 移除WEB应用容器

```
[docker@localhost ~]$ docker rm modest_banach
```

**注：**移除容器时，容器必须是停止状态。

## Docker镜像的使用

### 查看本地镜像列表

```
[docker@localhost ~]$ docker images
REPOSITORY          TAG                 IMAGE ID            CREATED             VIRTUAL SIZE
```

各个选项说明：

- REPOSITORY: 表示镜像的仓库源
- TAG: 镜像的标签
- IMAGE ID: 镜像ID
- CREATED: 镜像创建时间
- SIZE: 镜像大小

同一个仓库源可以有多个TAG，代表这个仓库源的不同个版本，如ubuntu仓库源里，有15.10,14.04等多个不同的版本，我们可以使用REPOSITORY:TAG来定义不同的镜像，如

```
[docker@localhost ~]$ docker run -t -i ubuntu:15.10 /bin/bash
```

如果不指定镜像的版本标签，docker将默认使用latest镜像

### 获取一个新的镜像

```
[docker@localhost ~]$ docker pull ubuntu:15.10
```
### 查找镜像

```
[docker@localhost ~]$ docker search httpd
NAME                          DESCRIPTION                                     STARS     OFFICIAL   AUTOMATED
```

NAME: 镜像仓库源的名称

DESCRIPTION: 镜像的描述

OFFICIAL: 是否是docker官方发布

### 拖取镜像

```
[docker@localhost ~]$ docker pull httpd
```
### 运行镜像

```
[docker@localhost ~]$ docker run httpd
```
### 自定义镜像

#### 创建镜像

当我们从docker镜像仓库中下载的镜像不能满足我们的需求时，我们可以通过以下两种方式对镜像进行更改：

1、从已经创建的容器中更新镜像，并且提交这个镜像

2、使用Dockerfile指令来创建一个新的镜像

#### 更新镜像

在更新镜像之前，我们先用以下命令启动容器，在容器中使用apt-get update命令更新，完成操作后使用exit退出容器。

```
[docker@localhost ~]$ docker run -t -i ubuntu:15.10 /bin/bash
root@2d60a31b8bdf:/# apt-get update
```
#### 提交容器副本

```
[docker@localhost ~]$ docker commit -m="has update" -a="ubuntu/update" 2d60a31b8bdf ubuntu:v2
ea547a1aa6de52e24092ff3ca13ae7ae58cd35123e2e58e6f3d784208af7ef5e
```

-m: 提交的描述信息

-a: 指定镜像作者

2d60a31b8bdf: 容器ID

runoob/ubuntu:v2: 指定要创建的目标镜像名

#### 构建镜像

创建Dockerfile，使用docker build命令来创建一个新的镜像

```
[docker@localhost docker]$ cat Dockerfile
FROM    centos:6.7
MAINTAINER      Fisher "artislong@haha.com"

RUN     /bin/echo 'root:123456' |chpasswd
RUN     useradd docker
RUN     /bin/echo 'docker:123456' |chpasswd
RUN     /bin/echo -e "LANG=\"en_US.UTF-8\"" >/etc/default/local
EXPOSE  22
EXPOSE  80
CMD     /usr/sbin/sshd -D
```

Dockerfile是一个文本格式的配置文件，它由一行行命令语句（指令）组成，并且支持以#开头的注释行

每个指令都会在镜像上创建一个新的层，每个指令的前缀都必须大写。

第一条FROM，指定使用哪个镜像源

RUN指令告诉docker在镜像内执行命令，安装了什么。。。

然后我们通过Dockerfile文件来构建一个镜像

```
[docker@localhost docker]$ docker build -t runoob/centos:6.7 .
```

千万不要忽略最后面的 “.”，它表示使用当前目录下的Dockerfile文件

-t: 指定要创建的目标镜像名

​

我们可以使用新的镜像来创建容器

```
[docker@localhost docker]$ docker run -t -i runoob/centos:6.7 /bin/bash
[root@ebd742bf9af0 /]# id docker
uid=500(docker) gid=500(docker) groups=500(docker)
```

从上面看到新镜像已经包含了我们创建的用户docker

#### 设置镜像标签

```
[docker@localhost docker]$ docker tag f38a8f197ee4 runoob/centos:dev
```

docker tag 镜像ID，镜像源名和新的标签名

## Docker安装Nginx

### 创建Nginx目录，用于存放后面相关文件

```
[docker@localhost ~]$ mkdir -p ~/nginx/www ~/nginx/logs ~/nginx/conf
```

www目录将映射为nginx容器配置的虚拟目录

logs目录将映射为nginx容器的日志目录

conf目录里的配置文件将映射为nginx容器的配置文件

### 查找Docker Hub上的nginx镜像

```
[docker@localhost nginx]$ docker search nginx
```

### 拉取官方nginx镜像

```
[docker@localhost nginx]$ docker pull nginx
```

### 查看nginx本地镜像

```
[docker@localhost nginx]$ docker images nginx
```

### 使用nginx镜像

#### 运行容器

```
[docker@localhost nginx]$ docker run -i -t -d -p 80:8081 --name nginx -v $PWD/www:/www -v $PWD/conf/nginx.conf:/etc/nginx/nginx.conf -v $PWD/logs:/wwwlogs  -d nginx
```

-p 80:8081: 将容器的80端口映射到宿主机的8081端口

-name nginx: 将容器命名为nginx

-v $PWD/www:/www: 将主机中当前目录下的www目录挂载到容器的/www

-v $PWD/conf/nginx.conf:/etc/nginx/nginx.conf: 将主机中当前目录下的nginx.conf挂载到容器的/etc/nginx/nginx.conf

-v $PWD/logs:/wwwlogs: 将主机中当前目录下的logs挂载到容器的/wwwlogs

#### 查看容器启动情况

```
[docker@localhost nginx]$ docker ps
```

#### 通过浏览器访问

访问路径为: http://主机ip:8081/ ，就可访问nginx

## Docker安装Tomcat

### 创建tomcat的相关目录

```
[docker@localhost ~]$ mkdir -p ~/tomcat/webapps ~/tomcat/logs ~/tomcat/conf
```

### 查找Docker Hub上的tomcat镜像

```
[docker@localhost ~]$ docker search tomcat
```

### 拉取官方tomcat镜像

```
[docker@localhost ~]$ docker pull tomcat
```

### 创建测试文件

- 在~/tomcat/webapps目录下创建test目录

  ~~~
  [docker@localhost webapps]$ mkdir test
  ~~~

- 进入test目录，编写测试页面

  ~~~
  [docker@localhost test]$ vi index.html
  ~~~

  index.html文件内容：

  ~~~html
  <html>
          <head>
                  <meta charset="utf-8">
                  <title>docker中的tomcat测试</title>
          </head>
          <body>
                  Hello, World! <br>
                  哈哈哈哈，运行成功啦啦啦啦啦
          </body>
  </html>
  ~~~

- 运行tomcat容器

  ~~~
  [docker@localhost tomcat]$ docker run --name tomcat -p 8080:8080 -v $PWD/webapps/test:/usr/local/tomcat/webapps/test -d tomcat
  ~~~

  命令说明：

  -v $PWD/webapps/test:/usr/local/tomcat/webapps/test: 将主机中当前目录下的test挂载到容器的/test

  启动成功后，在浏览器访问：http://主机ip:8080/test/index.html即可访问刚才编写的测试页面


---------------------------------------------------------------------------分割线-----------------------------------------------------------------------

# Dockerfile镜像制作

Dockerfile是一个文本格式的配置文件，用户可以使用Dockerfile快速创建自定义的镜像。

## 基本结构

Dockerfile由一行行命令语句组成，并且支持以#开头的注释行。

一般Dockerfile文件分为四部分：基础镜像信息、维护者信息、镜像操作指令和容器启动时执行指令。

```dockerfile
# This dockerfile uses the ubuntu image
# VERSION 2 - EDITION 1
# Author:docker_user
# Command format: Instruction [arguments / command] ..
# 第一行必须制定基于的基础镜像
FROM ubuntu
# 维护者信息
MAINTAINER docker_user docker_user@email.com
# 镜像的操作指令
RUN echo "deb http://archive.ubuntu.com/ubuntu/ raring main universe" >> /etc/apt/sources.list
RUN apt-get update && apt-get install -y nginx
RUN echo "\ndaemon off;" >> /etc/nginx/nginx.conf
# 容器启动时执行指令
CMD /usr/sbin/nginx
```

Dockerfile文件编写时，一开始必须指明所基于的镜像名称，接下来一般会说明维护者信息

后面则是镜像操作指令，例如RUN指令，镜像增加新的一层，并提交。最后是CMD指令，来指定运行容器时的操作命令。

以下有两个摘自书上的Dockerfile例子：

```dockerfile
# Ngnix
# 
# VERSION 0.0.1
FROM ubuntu
MAINTAINER Victor Vieux <victor@docker.com>
RUN apt-get update && apt-get install -y inotify-tools nginx apache2 openssh-server
```

此Dockerfile文件是在ubuntu父镜像基础上安装inotify-tools、nginx、apache2、openssh-server软件，从而创建一个新的Nginx镜像

**注** ：inotify-tools是为linux下inotify文件监控工具提供的一套c的开发接口库函数，同时还提供了一系列的命令行工具，这些工具可以用来监控文件系统的事件。

~~~dockerfile
# Firefox over VNC
# 
# VERSION 0.3
FROM ubuntu
# Install vnc, xvfb in order to reate a 'fake' display and firefox
RUN apt-get update && apt-get install -y xllvnc xvfb firefox
RUN mkdir /.vnc
# Setup a password
RUN xllvnc -storepasswd 1234 ~/.vnc/passwd
# Autostart firefox (might not be the best way, but it does the trick)
RUN bash -c 'echo "firefox" >> /.bashrc'
EXPOSE 5900
CMD ["xllvnc", "-forever", "-usepw", "-create"]
~~~

此Dockerfile基于ubuntu父镜像，安装filefox和vnc软件，启动后，用户可以通过5900端口通过vnc方式使用firefox。

## 指令

指令的一般格式为INSTRUCTION arguments，指令包括FROM、MAINTAINER、RUN等。

Dockerfile指令说明

|    指令     |                             说明                             |
| :---------: | :----------------------------------------------------------: |
|    FROM     |                   指定所创建镜像的基础镜像                   |
| MAINTAINER  |                        指定维护者信息                        |
|     RUN     |                           运行命令                           |
|     CMD     |                 指定启动容器时默认执行的命令                 |
|    LABEL    |                 指定生成镜像的元数据标签信息                 |
|   EXPOSE    |                  声明镜像内服务所监听的端口                  |
|     ENV     |                       指定容器环境变量                       |
|     ADD     | 复制指定的 \<src\> 路径下的内容到容器中的 \<dest\> 路径下，\<src\> 可以为URL；如果为tar文件，会自动解压到 \<dest\> 路径下 |
|    COPY     | 复制本地主机的 \<src\> 路径下的内容到镜像中的 \<dest\> 路径下；一般情况下推荐使用COPY而不是ADD |
| ENTRYPOINT  |                      指定镜像的默认入口                      |
|   VOLUME    |                       创建数据卷挂载点                       |
|    USER     |                 指定运行容器时的用户名或UID                  |
|   WORKDIR   |                         配置工作目录                         |
|     ARG     |          指定镜像内使用的参数 （例如版本号信息等）           |
|   ONBUILD   | 配置当所创建的镜像作为其他镜像的基础镜像时，所执行的创建操作指令 |
| STOPSIGNAL  |                       容器退出的信号值                       |
| HEALTHCHECK |                       如何进行健康检查                       |
|    SHELL    |                指定使用shell时的默认shell类型                |

### FROM

~~~dockerfile
格式为 FROM <image> 或FROM<image>:<tag>
~~~

第一条指令必须为FROM指令。并且，如果在同一个Dockerfile中创建多个镜像时，可以使用多个FROM指令（每个镜像一次）。

### MAINTAINER

~~~dockerfile
格式为 MAINTAINER <name>, 指定维护者信息
~~~
### RUN

~~~dockerfile
格式为 RUN <command> 或 RUN ["executable", "param1", "param2"]
~~~

RUN <command> 将在shell终端中运行命令，即 /bin/sh -c

RUN ["executable", "param1", "param2"]则使用exec执行。

指定使用其他终端可以通过第二种方式实现，例如 RUN ["/bin/bash", "-c", "echo hello"]。

每条RUN指令将在当前镜像基础上执行指令命令，并提交为新的镜像。当命令较长时可以用 \ 来换行。

### CMD

支持三种格式

- 使用exec执行，推荐方式

  ~~~dockerfile
  CMD ["executable", "param1", "param2"]
  ~~~

- 在/bin/sh中执行，提供给需要交互的应用

  ~~~dockerfile
  CMD command param1 param2
  ~~~

- 提供给ENTRYPOINT的默认参数

  ~~~dockerfile
  CMD ["param1", "param2"]
  ~~~

指定启动容器时执行的命令，每个Dockerfile只能有一条CMD命令。如果指定了多条命令，只有最后一条会被执行。

如果用户启动容器时指定了运行的命令，则会覆盖掉CMD指定的命令。

### EXPOSE

~~~dockerfile
格式为 EXPOSE <port> [<port>...]
~~~

例如：EXPOSE 22 80 8443

就是告诉Docker服务器容器暴露的端口号，供互联系统使用。在启动容器时需要通过-P或者-p来指定端口映射。

### ENV

~~~dockerfile
格式为 ENV <key> <value>
~~~

指定一个环境变量，会被后续RUN指令使用，并在容器运行时保持。例如：

~~~dockerfile
ENV PG_MAJOR 9.3
ENV PG_VERSION 9.3.4
RUN curl -SL http://example.com/postgres-$PG_VERSION.tar.xz | tar -xJC /usr/src/postgress && ...
ENV PATH /usr/local/postgres-$PG_MAJOR/bin:$PATH
~~~
### ADD

~~~dockerfile
格式为 ADD <src> <dest>
~~~

该命令将复制指定的<src>到容器中的<dest>。其中<src>可以是Dockerfile所在目录的一个相对路径（文件或目录）；也可以是一个URL；还可以是一个tar文件（自动解压为目录）。

### COPY

~~~dockerfile
格式为 COPY <src> <dest>
~~~

复制本地主机的<src>（为Dockerfile所在目录的相对路径，文件或目录）为容器中的<dest>。目标路径不存在时，会自动创建。

当使用本地目录为源目录时，推荐使用COPY

### ENTRYPOINT

- 使用exec执行，推荐方式

  ~~~dockerfile
  ENTRYPOINT ["executable", "param1", "param2"]
  ~~~

- 在shell中执行

  ~~~dockerfile
  ENTRYPOINT command param1 param2
  ~~~

配置容器启动后执行的命令，并且不可被docker run提供的参数覆盖

每个Dockerfile中只能有一个ENTRYPOINT，当制定多个ENTRYPOINT时，只有最后一个生效。

### VOLUME

~~~dockerfile
格式为 VOLUME ["/data"]
~~~

创建一个可以从本地主机或其他容器挂载的挂载点，一般用来存放数据库和需要保持的数据等。

### USER

~~~dockerfile
格式为 USER daemon
~~~

指定运行容器时的用户名或UID，后续的RUN也会使用指定的用户。

当服务不需要管理员权限时，可以通过该命令指定运行用户。并且可以在之前创建所需要的用户，例如：

~~~dockerfile
RUN groupadd -r postgres && useradd -r -g postgres postgres
~~~

要临时获取管理员权限可以使用gosu，不推荐sudo

### WORKDIR

~~~dockerfile
格式为 WORKDIR /path/to/workdir
~~~

为后续RUN、CMD、ENTRYPOINT指令配置工作目录。

可以使用多个WORKDIR指令，后续命令如果参数是相对路径，则会基于之前命令指定的路径。例如:

~~~dockerfile
WORKDIR /a
WORKDIR b
WORKDIR c
RUN pwd
~~~

上面指令最终结果为：/a/b/c

### ONBUILD

~~~dockerfile
格式为 ONBUILD [INSTRUCTION]
~~~

配置当所创建的镜像作为其他新创建镜像的基础镜像时，所执行的操作指令。例如：

~~~dockerfile
[...]
ONBUILD ADD . /app/src
ONBUILD RUN /usr/local/bin/python-build --dir /app/src
[...]
~~~

Dockerfile使用上面的内容创建了镜像image-A，如果基于image-A创建新的镜像时，新的Dockerfile中使用FROM image-A指定基础镜像时，会自动执行ONBUILD指令内容，等价于在Dockerfile后面添加了两条指令，如：

~~~dockerfile
FROM image-A
# Automatically run the following
ADD . /app/src
RUN /usr/local/bin/python-build --dir /app/src
~~~

使用ONBUILD指令的镜像，推荐在标签中注明，例如ruby:1.9-onbuild。

# 高级知识

## 资源隔离

Linux内核从2.4.19开始引入namespace的概念，其目的是将某个特定的全局系统资源（global system resource）通过抽象方法使得namespace中的进程看起来拥有它们自己的隔离的全局系统资源实例。

| namespace | 系统调用参数  | 隔离内容                   | 在容器语境下的隔离效果                                       |
| --------- | ------------- | -------------------------- | ------------------------------------------------------------ |
| UTS       | CLONE_NEWUTS  | 主机名和域名               | 每个容器可以有自己的hostname和domainname                     |
| IPC       | CLONE_NEWIPC  | 信号量、消息队列和共享内存 | 每个容器有其自己的System V IPC和POSIX消息队列文件系统，因此，只有在同一个IPC的进程之间才能互相通信 |
| PID       | CLONE_NEWPID  | 进程编号                   | 每个PID中的namespace中的进程可以有其独立的PID；每个容器可以有其PID为1的root进程；也使得容器可以在不同的host之间迁移，因为namespace中的进程ID和host无关了。这也使得容器中的每个进程有两个PID：容器中的PID和host上的PID |
| Network   | CLONE_NEWNET  | 网络设备、网络栈、端口等   | 每个容器都有其独立的网络设备，IP地址，IP路由表，/proc/net目录，端口号等。这也使得多个容器内的同一个应用都绑定在各自容器的80端口上 |
| Mount     | CLONE_NEWNS   | 挂载点（文件系统）         | 每个容器能看到不同的文件系统层次结构                         |
| User      | CLONE_NEWUSER | 用户和组ID空间             | 在User中的进程的用户和组ID可以和在host上不通。每个container可以有不同的user和group id；一个host上的非特权用户可以成为User中的特权用户 |

Docker的资源隔离也是通过这六种方式实现的，在容器启动时，Docker会创建这六种namespace实例，然后把容器中的所有进程放到这些namespace中，使得Docker容器中只能看到隔离的系统资源。

## 网络模式

docker目前支持四种网络工作的方式，分别为host，container，none，bridge。下面简单介绍下这几种网络模式。

- host模式

  Docker使用了Linux的Namespaces技术来进行资源隔离，如网卡、路由、进程等。如果启动容器的时候使用host模式，那么容器不会自己去创建一个独立的Network Namespace，而是与主机共用一个Network Namespace。容器也不会虚拟出自己的网卡、IP等，而是使用宿主机的IP和端口。

- container模式

  container模式指定创建的新的容器和已经存在的一个容器共享一个Network Namespace。container模式通过-net=container:NAME_OR_ID指定共享的容器。

- none模式

  在这种模式下，容器拥有自己的Network Namespace，但是不做任何网络配置，需要我们自己给容器添加网卡、IP等。

- bridge模式

  bridge模式是Docker默认的网络设置，此模式会为每一个容器分配Network Namespace、设置IP等，并将一个主机上的Docker容器连接到虚拟网桥上，实现容器和容器的主机的互连。​