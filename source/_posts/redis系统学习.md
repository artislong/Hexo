---

layout: post

title: redis常用配置

toc: true

date: 2018-02-08 22:51:33

tags: Redis

categories: Redis

description:

---


# Redis

## 1、基本配置

> Redis默认不是以守护进程的方式运行，可以通过该配置项修改，使用yes启用守护进程
<!--more-->
~~~properties
daemonize no
~~~

> 当Redis以守护进程方式运行时，Redis会默认把pid写入/var/run/redis.pid文件，可以通过pidfile指定

~~~properties
pidfile /var/run/redis.pid
~~~

> 指定Redis监听端口，默认端口为6379

~~~properties
port 6379
~~~

> 绑定主机地址

~~~properties
bind 127.0.0.1
~~~

> 当客户端闲置多长时间后关闭连接，如果指定为0，表示关闭该功能

~~~properties
timeout 300
~~~

> 指定日志记录级别，Redis总共支持四个级别：debug、verbose、notice、warning，默认为verbose

~~~properties
loglevel verbose
~~~

> 日志记录方式，默认为标准输出，如果配置Redis为守护进程方式运行，而这里又配置为日志记录方式为标准输出，则日志将会发送给/dev/null

~~~properties
logfile stdout
~~~

> 设置数据库的数量，默认数据库有16个（0-15），默认使用0，可以使用SELECT \<dbid\> 命令在连接上指定数据库id

~~~properties
databases 16
~~~

> 指定在多长时间内，有多少次更新操作，就将数据同步到数据文件，可以多个条件配合

~~~properties
save <seconds> <changes>
~~~

> Redis默认配置文件中提供了三个条件

~~~properties
save 900 1
save 300 10
save 60 10000
~~~

分别表示900秒（15分钟）内有1个更改，300秒（5分钟）内有10个更改以及60秒内有10000个更改

> 指定存储至本地数据库时是否压缩数据，默认为yes，Redis采用LZF压缩，如果为了节省CPU时间，可以关闭该选项，但是会导致数据库文件变大

~~~properties
rdbcompression yes
~~~

> 指定本地数据库文件名，默认值为dump.rdb

~~~properties
dbfilename dump.rdb
~~~

> 指定本地数据库存放目录

~~~properties
dir ./
~~~

> 设置当本机为slav服务时，设置master服务的IP地址及端口，在Redis启动时，它会自动从master进行数据同步

~~~properties
slaveof <masterip> <masterport>
~~~

> 当master服务设置了密码保护时，slav服务连接master的密码

~~~properties
masterauth <master-password>
~~~

> 如果配置了连接密码，客户端在链接Redis时需要通过AUTH \<password\>命令提供密码，默认关闭

~~~properties
requirepass foobared
~~~

> 设置同一时间最大客户端连接数，默认无限制，Redis可以同时打开的客户端连接数为Redis进程可以打开的最大文件描述符数，如果设置maxclients 0，表示不做限制。当客户端连接数达到限制时，Redis会关闭新的连接并像客户端返回max number of clients reached错误信息

~~~properties
maxclients 128
~~~

> 指定Redis最大内存限制，Redis在启动时会把数据加载到内存中，达到最大内存后，Redis会先尝试清除已到期或即将到期的Key，当此方法处理后，仍然达到最大内存限制，将无法再进行写操作，但仍然可以进行读取操作。Redis新的vm机制，会把Key存放内存，Value会存放到swap区

~~~properties
maxmemory <bytes>
~~~

> 指定是否在每次更新操作后进行日志记录，Redis在默认情况下是异步的把数据写入磁盘，如果不开启，可能会在断电时导致一段时间内的数据丢失。因为redis本身同步数据文件是按上面的save条件同步的，所以有的数据会在一段时间内只存在于内存中，默认为no

~~~properties
appendonly no
~~~

> 指定更新日志文件名，默认为appendonly.aof

~~~properties
appendfilename "appendonly.aof"
~~~

> 指定更新日志条件，共有3个可选值

no：表示等操作系统进行数据缓存同步到磁盘（快）

always：表示每次更新操作后手动调用fsync() 将数据写到磁盘（慢，安全）

everysec：表示每次同步一次（折衷，默认值）

~~~properties
appendfsync everysec
~~~

> 指定是否启用虚拟内存机制，默认值为no，VM机制将数据分页存放，由Redis将访问量较少的页即冷数据swap到磁盘上，访问多的页面由磁盘自动缓存到内存中

~~~properties
vm-enabled no
~~~

> 虚拟内存文件路径，默认值为/tmp/redis.swap，不可多个Redis实例共享

~~~properties
vm-swap-file /tmp/redis.swap
~~~

> 将所有大于vm-max-memory的数据存入虚拟内存，无论vm-max-memory设置多小，所有索引数据都是内存存储的（Redis的索引数据就是keys），也就是说，当vm-max-memory设置为0的时候，其实是所有value都存在于磁盘。默认值为0

~~~properties
vm-max-memory 0
~~~

> Redis swap文件分成了很多的page，一个对象可以保存在多个page上面，但一个page上不能被多个对象共享，vm-page-size是要根据存储的数据大小来设定的，如果存储很多小对象，page大小最好设置为32或者64bytes，如果存储很多的大对象，则可以使用更大的page，如果不确定，就使用默认值

~~~properties
vm-page-size 32
~~~

> 设置swap文件中的page数量，由于页表（一种表示页面空闲或使用的bitmap）是存放在内存中的，在磁盘上每8个pages将消耗1byte的内存

~~~properties
vm-pages 134217728
~~~

> 设置访问swap文件的线程数，最好不要超过机器的核数，如果设置为0，那么所有对swap文件的操作都是串行的，可能会造成比较长时间的延迟，默认值为4

~~~properties
vm-max-threads 4
~~~

> 设置在向客户端应答时，是否把较小的包合并为一个包发送，默认为开启

~~~properties
glueoutputbuf yes
~~~

> 指定在超过一定的数量或者最大的元素超过某一临界值时，采用一种特殊的哈希算法

~~~properties
hash-max-zipmap-entries 64
hash-max-zipmap-value 512
~~~

> 指定是否激活重置哈希，默认为开启

~~~properties
activerehashing yes
~~~

## 2、常用操作（高级）

[Redis官网命令详解](https://redis.io/commands)

### 2.1 事务操作

Redis的事务也有两种，乐观锁和悲观锁。

悲观锁，直接给这个key加锁，这个key只能当前连接可以操作。

乐观锁，给这个key加锁，只要这个key的值没有更改就可以了。

Redis的事务中，默认启用的是乐观锁，只负责监测key有没有变动。

~~~properties
watch key1 key2  监听key有没有变化，如果有变化，则事务取消，在multi之前使用
unwatch key1 key2  不加具体key时，取消所有key的监听。
multi  开启事务
...
discard/exec  取消事务/提交事务
~~~

注意：如果在exec时，命令语法有问题，这时所有的语句都得不到执行。如果语法本身没问题，但是适用的对象有问题，exec之后，会执行正确的语句，并跳过有问题的语句。

### 2.2 消息的发布与订阅

使用办法：

订阅端：subscribe  频道名称

psubscribe 支持表达式匹配的频道

返回值为订阅到消息的客户端数量

发布端：publish  频道名称  发布内容

### 2.3 Redis持久化配置

> 持久化

把数据存储于断电后不会丢失的设备中，通常是硬盘

> 常见的持久化方式

主从：通过主服务器往内存中写，从服务器做保存和持久化

日志：操作生成相关日志，并通过日志来恢复数据，couchDB对于数据内容，不修改，只追加，则文件本身就是日志，不会丢失数据。

#### 2.3.1 rdb快照持久化

> rdb工作原理

每隔N分钟或N次写操作后，从内存dump数据形成rdb文件，压缩放在备份目录

> rdb快照相关参数

~~~properties
save 900 1 #刷新快照到磁盘中，必须满足两者要求才会触发，即900秒之内至少1个关键字发生变化
save 300 10 #必须是300秒之内至少10个关键字发生变化
save 60 10000 #必须是60秒之内至少10000个关键字发生变化
stop-writes-on-bgsave-error yes #后台存储错误停止写rdb
rdbcompression yes #使用LZF压缩rdb文件
rdbchecksum yes #导入rdb恢复时数据时，要不要校验rdb的完整性
dbfilename dump.rdb #设置rdb文件名
dir ./ #设置工作目录，rdb文件会写入该目录
~~~

> rdb的缺陷

在上一个保存点刚结束，下个保存点还没到时如果断电，将会丢失1-N分钟的数据

> rdb的优势

由于导出的是一个内存的二进制文件，所以rdb文件的恢复速度超级快

#### 2.3.2 aof日志持久化

> aof工作原理

redis客户端连接redis服务器后所进行的每一条命令的操作都逐条记录到aof日志中，在恢复数据时，只需要将日志中记录的命令都依次执行一遍即可。

> aof配置参数

~~~properties
appendonly yes #是否打开aof日志功能
appendfilename "appendonly.aof"  #设置aof文件名
appendfsync everysec #折衷方案，每秒写一次
appendfsync no #写入工作交给操作系统，由操作系统判断缓冲区大小，统一写入到aof，同步频率低，速度快
appendfsync always #每一个命令都立即同步到aof，安全，速度慢
no-appendfsync-on-rewrite no #正在到处rbd快照的过程中，要不要停止同步aof
auto-aof-rewrite-percentage 100 #aof文件大小比起上次重写时的大小，增长率100%时进行重写
auto-aof-rewrite-min-size 64mb  #aof文件，至少超过64M时进行重写
# 上面两个aof重写规则同时满足aof才会进行重写
~~~

> aof的缺陷

由于项目使用redis，就是图redis的读写速度快，但是如果频繁写磁盘，也会拉低效率的，而且越往后，aof日志文件会越来越大的。

> aof的优势

使用日志记录操作，能有效的保证数据的完整性，就算太巧合了，在刚执行了命令还没来得及同步aof时断电了，那也只会丢失当前的一条命令。

> aof重写

把内存中的数据，逆化成命令写到aof日志里，以解决aof日志过大的问题。

#### 2.3.3 总结

1、在dump过程中，aof如果停止同步，数据不会丢失，因为所有的操作会缓存在队列里，dump完成后，统一操作

2、如果rdb和aof文件都存在，它会采用谁优先就用谁来恢复数据，也就是会用aof

3、对于rdb和aof两种持久化方式，没有绝对的谁好谁坏，所以可以两者同时用，效果更优

4、rdb和aof相比，rdb的数据恢复更快，因为rdb的数据是内存映射，可以直接载入到内存，而aof是一条条的命令，需要逐条执行

### 2.4 Redis主从复制

此处使用 4.0.2 的版本，搭建一主两从的Redis集群。

> 环境准备

从[此处](http://download.redis.io/releases/redis-4.0.2.tar.gz)下载redis-4.0.2.tar.gz，解压之后，编译源码进行安装。

~~~shell
$ wget http://download.redis.io/releases/redis-4.0.2.tar.gz
$ tar -zxvf redis-4.0.2.tar.gz
$ cd redis-4.0.2
$ make && make install
~~~

在用户家目录下新建redis相关目录

```shell
$ cd ~
$ make redis
$ cd redis
$ mkdir master
# mkdir slave
```

然后拷贝redis-4.0.2目录中的redis.conf配置文件到master，修改为redis_master.conf，slave中拷贝两份，分别命名为redis_slave1.conf，redis_slave2.conf。

接下来修改配置文件内容（只贴出关键几个点，其他辅助内容请读者自行修改）：redis_master.conf

~~~properties
port 7000
daemonize yes
~~~

redis_slave1.conf

~~~properties
port 7001
daemonize yes
slaveof localhost 7000
~~~

redis_slave2.conf

```properties
port 7002
daemonize yes
slaveof localhost 7000
```

分别启动三个节点：

~~~shell
$ ./redis-4.0.2/src/redis-server ./master/redis_master.conf
$ ./redis-4.0.2/src/redis-server ./slave/redis_slave1.conf
$ ./redis-4.0.2/src/redis-server ./slave/redis_slave2.conf
~~~

登录进master节点，查看主从模式是否正常启动(主节点上能显示出两个从节点即可)

~~~shell
$ ./redis-4.0.2/src/redis-cli
> info replication
~~~

### 2.5 三主三从三哨兵集群模式

redis编译安装和上面一样

#### 2.5.1 环境准备

master

```
192.168.10.100:6380,192.168.10.100:6381,192.168.10.100:6382
```

slave

```
192.168.10.100:6383,192.168.10.100:6384,192.168.10.100:6385
```

sentinel

```
192.168.10.100:26380,192.168.10.100:26381,192.168.10.100:26382
```

#### 2.5.2 修改配置文件

手动在服务器上新建6380,6381,6382,6383,6384,6385几个目录，将redis.conf配置文件每个目录拷贝一份（批量拷贝文件时，最好参考linux中xargs命令；当然，也可以cp多执行几次）。

redis.conf配置文件修改

```properties
# 端口分别为6380,6381,6382,6383,6384,6385
port 6380
# 默认端口为127.0.0.1，改为本机地址则为任意服务器都可以访问，若只指定服务器访问，则改为指定服务器ip即可，由于当前是一台服务器上的伪集群，所以配置本机ip地址。
bind 192.168.10.100
# Redis后台运行
daemonize yes
# pidfile文件对应存放目录(redis节点进程号)
pidfile /home/admin/redis/cluster/6380/redis.pid
# 操作日志
logfile "/home/admin/redis/cluster/6380/redis.log"
# 数据文件存放目录
dir /home/admin/redis/cluster/6380/
# 是否开启集群(重点)
cluster-enabled yes
# 集群节点配置，集群首次启动自动生成
cluster-config-file nodes.conf
# 集群节点连接超时时间
cluster-node-timeout 15000
# aof日志开启，可做为日志记录，也可借此恢复数据
appendonly yes
```

其他节点类似。

然后启动每一个节点

```shell
$ ./redis-4.0.2/src/redis-server ./cluster/7000/redis.conf
$ ./redis-4.0.2/src/redis-server ./cluster/7001/redis.conf
$ ./redis-4.0.2/src/redis-server ./cluster/7002/redis.conf
$ ./redis-4.0.2/src/redis-server ./cluster/7003/redis.conf
$ ./redis-4.0.2/src/redis-server ./cluster/7004/redis.conf
$ ./redis-4.0.2/src/redis-server ./cluster/7005/redis.conf
```

#### 2.5.3 创建集群

这里创建集群借助于redis自己提供的一个集群创建工具redis-trib.rb（依赖于ruby环境）

```shell
$ ./redis-4.0.2/src/redis-trib.rb create --replicas 1 192.168.10.100:6380 192.168.10.100:6381 192.168.10.100:6382 192.168.10.100:6383 192.168.10.100:6384 192.168.10.100:6385
```

根据提示完成集群创建

接下来验证集群是否搭建成功，连接其中一个节点

```shell
$ ./redis-4.0.2/src/redis-cli -h 192.168.10.110 -p 6380
> cluster nodes
dcebbc47abd482363f221020dd1be714a498b841 192.168.10.110:6382 master - 0
1524568524063 3 connected 10923-16383
33eaf24ca33b4f5bcc37c2a7434bddaf5432f057 192.168.10.110:6380 myself,master - 0 0 1
connected 0-5460
51ed0849bf29de0221eb3b9b4ccbebfd341593ad 192.168.10.110:6384 slave
409b146c1fc86acfd6198c491cf77eaf8c8c7c04 0 1524568525066 5 connected
e29d4537e924f0e29f5155536a636b35a44a8c24 192.168.10.110:6383 slave
33eaf24ca33b4f5bcc37c2a7434bddaf5432f057 0 1524568523562 4 connected
dc4f070c42f9002e1c54bb6019ee4c34331570cc 192.168.10.110:6385 slave
dcebbc47abd482363f221020dd1be714a498b841 0 1524568523062 6 connected
409b146c1fc86acfd6198c491cf77eaf8c8c7c04 192.168.10.110:6381 master - 0
1524568524063 2 connected 5461-10922
```

输入info replication可查看节点信息。

#### 2.5.4 哨兵搭建

新建sentinel/26380,26381,26382目录，将redis家目录下的sentinel.conf文件拷贝到每个目录中

sentinel.conf

```properties
bind 192.168.10.100
port 26380
daemonize yes
dir "/home/admin/redis/cluster/sentinel/26380"
# 故障转移配置
# 表示哨兵集群中，至少有两个节点认为Redis节点挂掉，则将节点从集群中剔除
sentinel monitor mymaster 192.168.10.100 6380 2
sentinel config-epoch mymaster 0
sentinel leader-epoch mymaster 0
```

其他的类似

启动哨兵

```shell
$ ./redis-4.0.2/src/redis-sentinel ./cluster/sentinel/26380/sentinel.conf
$ ./redis-4.0.2/src/redis-sentinel ./cluster/sentinel/26381/sentinel.conf
$ ./redis-4.0.2/src/redis-sentinel ./cluster/sentinel/26382/sentinel.conf
```

查看哨兵节点信息

```shell
$ ./redis-4.0.2/src/redis-cli -h 192.168.10.110 -p 26380
> info Sentinel
sentinel_masters:1
sentinel_tilt:0
sentinel_running_scripts:0
sentinel_scripts_queue_length:0
sentinel_simulate_failure_flags:0
master0:name=cpicmaster,status=ok,address=192.168.10.110:6380,slaves=1,sentinels=3
```



